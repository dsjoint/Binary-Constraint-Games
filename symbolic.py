import numpy as np
from sympy import *
from sympy.solvers import solve
from sympy.abc import a, b, c, d, e

"""
Failed attempt at using symbolic computation to find unsolvable systems of equations.
This method is very inefficent. 
"""

# Power Set
def ps(A):
    def powerset(A):
        if A == []:
            yield []
        else:
            a = A[0]
            for tail in powerset(A[1:]):
                yield tail
                yield [a] + tail
    A = list(powerset(A))
    A.remove([])
    return A

# Get equations in a set of variables
def get_eqs(var):
    eqs = ps(var)
    all_eq = []
    for i in [0,1]:
        for eq in eqs:
            x = 1
            for var in eq:
                x *= var
            x -= i
            all_eq.append(x)
    all_eq = set([frozenset([eq]) for eq in all_eq])
    return all_eq

# Find unsolvable systems
def get_sys():
	eqs = get_eqs([a,b])
	solvable = set([eq for eq in eqs])
	unsolvable = set()

	while (len(solvable) > 0):
	    sys = solvable.pop()
	    for eq in eqs:
	        if(list(eq)[0] not in list(sys)):
	            new_sys = sys.union(eq)
	            if(len(solve(new_sys,a,b)) == 0):
	                unsolvable.add(new_sys)     
	            elif(len(new_sys)):
	                solvable.add(new_sys)
	    print(len(unsolvable))
	    return unsolvable
