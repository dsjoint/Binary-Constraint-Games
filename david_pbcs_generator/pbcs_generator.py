import fieldmath
import numpy as np
import sympy
import qecc as q
import itertools
import random
import time
import datetime
import multiprocessing as mp

def generate_index_permutations(n, r):
    index = [i for i in range(n)]
    return itertools.permutations(index, r)

def gen(n, m):
    for i in range(2**(n*m)):
        yield np.array([int(k) for k in "{0:b}".format(i).zfill(n*m)]).reshape(n,m)

def generate_matrix(n, m):
    '''
    Return all possible m by n matrices over F_2, where n represents the number of variables and m represents the
    number of constraints.
    '''
    L = list(gen(m, n))
    if n == 1:
        return [i.tolist()[0] for i in L]
    else:
        return [mat.tolist() for mat in L]

def generate_constraint(m):
    b = list(gen(1, m))
    return [i.tolist()[0] for i in b]

def augment(A, b):
    '''
    Return an augmented matrix for the system Ax = b.
    '''
    if len(A) != len(b):
        raise ValueError("Invalid number of rows")

    aug = [A[i] + [b[i]] for i in range(len(b))]

    return aug

def unaugment(B):
    A = [a[:-1] for a in B]
    b = [a[-1] for a in B]

    return A, b

def detect_solutions(A, b):
    '''
    Return if there exists a solution to the system Ax = b.

    >>> detect_solutions([[1, 1, 1, 0, 0, 0, 0, 0, 0], [0, 0, 0, 1, 1, 1, 0, 0, 0], [0, 0, 0, 0, 0, 0, 1, 1, 1], [1, 0, 0, 1, 0, 0, 1, 0, 0], [0, 1, 0, 0, 1, 0, 0, 1, 0], [0, 0, 1, 0, 0, 1, 0, 0, 1]], [0, 0, 0, 0, 0, 1])
    False
    '''

    if len(A) != len(b):
        raise ValueError("Invalid number of rows")

    inp = augment(A, b)
    field = fieldmath.PrimeField(2)

    mat = fieldmath.Matrix(len(inp), len(inp[0]), field)

    for i in range(mat.row_count()):
        for j in range(mat.column_count()):
            mat.set(i, j, inp[i][j])

    mat.reduced_row_echelon_form()

    zero = True
    for j in range(mat.column_count()-1):
        if mat.get(mat.row_count()-1, j) == 1:
            zero = False

    if zero and mat.get(mat.row_count()-1, mat.column_count()-1) == 1:
        return False
    else:
        return True

# not being used
def sympy_detect_solutions(A, b):
    if len(A) != len(b):
        raise ValueError("Invalid number of rows")

    inp = augment(A, b)

    rows = len(inp)
    cols = len(inp[0])

    mat = sympy.Matrix(np.array(inp))

    mat = mat.rref()[0]

    zero = True
    for j in range(cols-1):
        if mat[rows-1, j] % 2 == 1:
            zero = False

    if zero and mat[rows-1, cols-1] % 2 == 1:
        return False
    else:
        return True

def find_unsatisfiable(n, m):
    '''
    Return all unsatisfiable binary constraint systems with n variables and m constraints.
    '''
    A_list = generate_matrix(n, m)
    b_list = generate_constraint(m)
    unsatisfiable_BCS = []

    for A in A_list:
        for b in b_list:
            if detect_solutions(A, b) == False:
                aug = augment(A, b)
                unsatisfiable_BCS.append(aug)

    return unsatisfiable_BCS

def q_solve(A, b, capacity, dim):

    G = []

    for i in range(capacity + 1):
        G.append(list(q.mutually_commuting_sets(i, dim)))

    iden = []
    neg_iden = []

    for i in range(len(G)):
        t_iden = []
        t_neg_iden = []
        for s in G[i]:
            prod = q.Pauli(dim * 'I')
            for j in range(i):
                prod = prod * s[j]
            if prod == q.Pauli(dim * 'I'):
                t_iden.append(s)
            elif prod == -q.Pauli(dim * 'I'):
                t_neg_iden.append(s)
        iden.append(t_iden)
        neg_iden.append(t_neg_iden)


    for i in range(len(iden)):
        t_iden = []
        t_neg_iden = []
        for a in neg_iden[i]:
            for j in range(i):
                t_neg_iden.append(a[j:] + a[:j])

        for a in iden[i]:
            for j in range(i):
                t_iden.append(a[j:] + a[:j])
        iden[i] = list(set(t_iden))
        neg_iden[i] = list(set(t_neg_iden))

    neg_constraints = []

    for i in range(len(b)):
        if b[i] == 1:
            neg_constraints.append(i)

    neg_poss = []

    def check_valid(assigned, strict):
        valid = True
        for i in range(len(A)):
            d_None = False
            indices = [j for j in range(len(A[i])) if A[i][j] == 1]
            prod = q.Pauli(dim * 'I')

            for j in indices:
                if assigned[j] == None:
                    d_None = True
                    break
                prod = prod * assigned[j]
            if d_None:
                if strict:
                    return False
                continue

            if b[i] == 0:
                if prod != q.Pauli(dim*'I'):
                    valid = False
            else:
                if prod != -q.Pauli(dim*'I'):
                    valid = False
        return valid

    def fill_neg(assigned, num_filled):
        if num_filled == len(neg_constraints):
            neg_poss.append(assigned)
            return

        for i in range(num_filled, len(neg_constraints)):
            indices = [j for j in range(len(A[neg_constraints[i]])) if A[neg_constraints[i]][j] == 1]
            num_a = len(indices)
            for a in neg_iden[num_a]:
                t_assigned = assigned[:]

                fits = True

                for j in range(num_a):
                    if a[j] != t_assigned[indices[j]] and t_assigned[indices[j]] != None:
                        fits = False
                        break
                    t_assigned[indices[j]] = a[j]

                if check_valid(t_assigned, False) and t_assigned != assigned and fits == True:
                    fill_neg(t_assigned, num_filled+1)

    assigned = len(A[0])*[None]
    fill_neg(assigned, 0)

    sol = []

    def bf(assigned, checked):
        if check_valid(assigned, True):
            sol.append(assigned)
            return

        for i in range(checked, len(A)):

            filled = True
            indices = [j for j in range(len(A[i])) if A[i][j] == 1]
            num_a = len(indices)

            for j in range(len(A[i])):
                if assigned[j] == None:
                    filled = False
            if filled:
                continue

            for a in iden[num_a]:

                t_assigned = assigned[:]

                fits = True

                for j in range(num_a):
                    if a[j] != t_assigned[indices[j]] and t_assigned[indices[j]] != None:
                        fits = False
                        break
                    t_assigned[indices[j]] = a[j]

                if check_valid(t_assigned, False) and t_assigned != assigned and fits == True:
                    bf(t_assigned, checked+1)

    t_neg_poss = set(tuple(x) for x in neg_poss)
    neg_poss = [list(x) for x in t_neg_poss]

    for a in neg_poss:
        bf(a, 0)

    return sol

def search(n, m, dim, mult_proc):
    '''
    Searches all systems with n variables and m constraints to find a pseudo-telepathic system.
    '''

    search_list = []
    l = find_unsatisfiable(n, m)
    print("list size:", len(l))

    for a in l:
        A, b = unaugment(a)
        search_list.append((A, b))

    '''
    for i in range(100):
        r = random.randint(0, len(l))
        A, b = unaugment(l[r])
        search_list.append( (A, b) )

    for a in search_list:
        for b in a[0]:
            print(b)
        print("---")
        print(a[1])
        print("===")
    '''

    found = False
    count = 0
    total_time = 0
    average_time = 0
    max_time = 0

    if mult_proc:
        num_workers = mp.cpu_count()
        tasks = [(a[0], a[1], n, dim) for a in search_list]

        pool = mp.Pool(processes=num_workers)
        results = pool.starmap(q_solve, tasks)
        for r in results:
            if len(r) > 0:
                found = True
    else:
        for a in search_list:
            if count > 0:
                print("Estimated time left:", datetime.timedelta(seconds = round(len(search_list)*average_time - total_time)))
            start = time.time()
            if len(q_solve(a[0], a[1], n, dim)) > 0:
                print(a[0], a[1])
                found = True
            end = time.time()
            time_taken = end-start
            if time_taken > max_time:
                max_time = time_taken
            total_time += time_taken
            count += 1
            average_time = total_time/count

    print(found)

#========================================

'''
X = q.Pauli('X')
Y = q.Pauli('Y')
Z = q.Pauli('Z')
I = q.Pauli('I')
'''

A = [[1, 1, 1, 0, 0, 0, 0, 0, 0], [0, 0, 0, 1, 1, 1, 0, 0, 0], [0, 0, 0, 0, 0, 0, 1, 1, 1], [1, 0, 0, 1, 0, 0, 1, 0, 0], [0, 1, 0, 0, 1, 0, 0, 1, 0], [0, 0, 1, 0, 0, 1, 0, 0, 1]]
b = [0, 0, 0, 1, 1, 1]

'''

A = [[1, 1, 1, 0, 0, 0, 0, 0, 0] + 18*[0] + 3*[0],
     [0, 0, 0, 1, 1, 1, 0, 0, 0] + 18*[0] + 3*[0],
     [0, 0, 0, 0, 0, 0, 1, 1, 1] + 18*[0] + 3*[0],
     [1, 0, 0, 1, 0, 0, 1, 0, 0] + 18*[0] + 3*[0],
     [0, 1, 0, 0, 1, 0, 0, 1, 0] + 18*[0] + 3*[0],
     [0, 0, 1, 0, 0, 1, 0, 0, 1] + 18*[0] + 3*[0],
     9*[0] + [1, 1, 1, 0, 0, 0, 0, 0, 0] + 3*[0],
     9*[0] + [0, 0, 0, 1, 1, 1, 0, 0, 0] + 3*[0],
     9*[0] + [0, 0, 0, 0, 0, 0, 1, 1, 1] + 3*[0],
     9*[0] + [1, 0, 0, 1, 0, 0, 1, 0, 0] + 3*[0],
     9*[0] + [0, 1, 0, 0, 1, 0, 0, 1, 0] + 3*[0],
     9*[0] + [0, 0, 1, 0, 0, 1, 0, 0, 1] + 3*[0],
     18*[0] + [1, 1, 1, 0, 0, 0, 0, 0, 0] + 3*[0],
     18 * [0] + [0, 0, 0, 1, 1, 1, 0, 0, 0] + 3*[0],
     18 * [0] + [0, 0, 0, 0, 0, 0, 1, 1, 1] + 3*[0],
     18 * [0] + [1, 0, 0, 1, 0, 0, 1, 0, 0] + 3*[0],
     18 * [0] + [0, 1, 0, 0, 1, 0, 0, 1, 0] + 3*[0],
     18 * [0] + [0, 0, 1, 0, 0, 1, 0, 0, 1] + 3*[0],
     27*[0] + [1, 1, 1]]

b = 18*[0] + [1]

'''

if __name__ == '__main__':
    start = time.time()

    #q_solve(A, b, 4, 2)

    # (4, 3) returned false
    # (3, 4) returned false

    search(3, 3, 2, True)

    end = time.time()

    print("Total elapsed time: ", datetime.timedelta(seconds=round(end-start)))

    start = time.time()

    search(3, 3, 2, False)

    end = time.time()

    print("Total elapsed time: ", datetime.timedelta(seconds=round(end-start)))