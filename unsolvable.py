import numpy as np

def multiply(a,b):
    m = 1
    for i in range(len(a)):
        if (int(a[i]) == 1):
            m *= int(b[i])
    return m

# Get matrix representing every equation in n variables on all possible inputs
def system_eq(n):
    L = np.zeros((2*(2**n - 1),2**n))
    form = '{0:0' + str(n) + 'b}'
    for res in [0,1]:
        for eq in range(1,2**n):
            for var in range(2**n):
                a,b = form.format(eq), form.format(var)
                r = multiply(a, b)
                L[(res*(2**n - 1))+(eq-1), var] = (r == res)
    return L

# Find all unsolvable equations in n variables 
# Returns list of binary indicies, binary indicies represnt system of equation
def unsolvable(n):
    unsolved = []
    L = system_eq(n)
    m = 2*(2**n - 1)
    form = '{0:0' + str(m) + 'b}'
    for i in range(2**m):
        b = [j == '1' for j in list(form.format(i))]
        if(L[b,:].prod(0).sum(0) > 0):
            unsolved.append(form.format(i))
    return unsolved

"""
TODO:
Make unsolvable(n) more efficent by only returning the minimum span of unsolvable systems of equations.

If f = \{ f1, f2,..., fn\} not satisfiable then f union fk is no longer considered 
"""


